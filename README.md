# correlation-septet

Just a picture that shows behaviour of different metrics of dependence.

Relevant files:

correlation-septet.png -- The figure

correlation septet.RMD -- R code to create the figure

Related blog post: [mattiheino.com/correlation](https://mattiheino.com/correlation)